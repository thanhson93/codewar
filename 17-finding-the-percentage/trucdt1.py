if __name__ == '__main__':
  n = int(input())
  student_marks = {}
  # Collect the student and their marks to student_marks list
  for _ in range(n):
    name, *line = input().split()
    scores = list(map(float, line))
    student_marks[name] = scores
  # Get the name of student who want to get it's average marks
  query_name = input()
  average_marks = 0
  # Total query_name student score
  for i in student_marks[query_name]:
    average_marks += i
  # Pints the average marks of query_name student
  print ("{0:.2f}".format(average_marks/(len(student_marks[query_name])),2))
