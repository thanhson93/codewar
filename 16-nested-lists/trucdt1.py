if __name__ == '__main__':
  arr = []
  arr_grade = []
  # Collect the inputs (name and it's grade) to arr and arr_grade list
  for _ in range(int(input())):
    name = input()
    score = float(input())
    arr.append([str(name),score])
    arr_grade.append(score)
  # Sort the arr_grade list and get the second highest grade
  second_highest = sorted(set(arr_grade))[1]
  # Find the name that matches the second highest grade
  for i,j in sorted(arr):
    if j == second_highest:
      # Print the name that matches the second highest grade
      print (i)