students = []
scores = []

for _ in range(int(raw_input())):
    name = raw_input()
    score = float(raw_input())
    students.append([name, score])
    scores.append(score)

scores = list(set(scores))

scores.sort()
students.sort()

for i in range(len(students)):
    if students[i][1] == scores[1]:
        print(students[i][0])
