from collections import Counter
# Enter your code   here. Read input from STDIN. Print output to STDOUT
if __name__ == "__main__":
    n = int(input()) 
    s = [int(i) for i in input().split()]
    
    shore_dict = Counter(s)
    total_earn = 0

    for i in range(int(input())):
        size, price = [int(x) for x in input().split()] 

        if size in shore_dict:       
            shore_dict[size] -= 1
            if shore_dict[size] == 0:
                shore_dict.pop(size) #remove when value of key is zero
            total_earn += price
    
    print(total_earn)