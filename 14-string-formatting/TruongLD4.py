# Because hex have special value so define the list for convert_factor function
Character = ('0','1','2','3','4','5','6','7','8','9','A', 'B', 'C', 'D', 'E', 'F')

# This function convert a integer number to the factor value (dec, oct, hex, bin)
# width value is used to calculate the space-padded for factor value 
#"Each value should be space-padded to match the width of the binary value of n"

def convert_factor(number,factor,width):
    s = ""
    while number > 0:
        s += Character[number % factor]  # get remainder 
        number //= factor                  
    
    if width > 0:
        s += ' '*(width - len(s)) # add space-pad

    return s[::-1] # inverses the string s

# This function just only print four factor values in range of number
def print_formatted(number):
    lth_pad = len(convert_factor(number,2,0)) #Gets the width of the binary value of n

    for i in range(1,number+1):
        dec_factor = convert_factor(i,10,lth_pad)
        oct_factor = convert_factor(i,8,lth_pad)
        hex_factor = convert_factor(i,16,lth_pad)
        bin_factor = convert_factor(i,2,lth_pad)

        print(dec_factor, oct_factor, hex_factor, bin_factor)


if __name__ == '__main__':
    n = int(input())
    print_formatted(n)