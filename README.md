# CodeWar project trainning
Solve the problem and prepare the knowledge Python to member, so that they can understand the source code and coding style. 
## Member
- SonNT62
- TruongLD4
- TrucDT1
## Note
- Please **DONT** solve the problem too long. you'all can ask me if you blocked.
- The time preparing for CodeWar Competition is **ONE** month.
- Deadline: 20 July 2019
## Instruction
### 1. Git instruction
- Git command
```
git clone <link repo>
git checkout <branch>
git pull
git push <branch>
git rebase <branch>
```
### Solving the problem
**Create issue:**
- Create issue on gitlab webpage.
- Add the problem on description on issue, assignee and then create branch.
- Checkout the branch created.

**Start working:**
- Create the folder the same with branch you created.
- Create README.md on the folder issue you work on. This file contains your note on the problem.
- Start solve the problem with code.
- Please add the link website to the problem.

**Commit the source**
- Use `git commit` to commit the file.
- If the problem solved completed, you can create merge request for review and merge to master


**STATUS**
- 25-10-2019: update the README.md follow the markdown standard. `No longer working.`